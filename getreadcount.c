#include "types.h"
#include "stat.h"
#include "user.h"

int 
main(int amount, char **commands)
{
  			//Only getreadcount
  if(amount == 1){

    printf(1, "Tracked command has been called %d times\n", getreadcount(-1));
  
  			//Reset counter
  }else if (atoi(commands[1]) == 0){

    printf(1, "Tracked command has been reset\n");

  			//Change tracked command
  }else{

    if(atoi(commands[1])<1 || atoi(commands[1])>22){
      printf(1, "Given number: %d doesn't match a system call\n", atoi(commands[1]));
      exit();}

    getreadcount(atoi(commands[1]));
    printf(1, "Command was changed\n");
  }

  exit();
}
